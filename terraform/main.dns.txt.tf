resource "azurerm_dns_txt_record" "hic" {
  zone_name           = "${azurerm_dns_zone.hic.name}"
  resource_group_name = "${azurerm_resource_group.dns.name}"
  ttl                 = "${var.default_ttl}"

  name = "@"

  record {
    value = "MS=ms30064265"
  }

  record {
    value = "v=spf1 include:spf.protection.outlook.com -all"
  }

  tags = "${var.tags}"
}
