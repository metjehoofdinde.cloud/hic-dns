variable "resource_prefix" {
  type    = "string"
  default = "hic"
}

variable "location" {
  type    = "string"
  default = "west europe"
}

variable "tags" {
  type = "map"

  default = {
    terrafrommanaged = "true"
  }
}

variable "dnsname" {
  type = "string"

  default = "metjehoofdinde.cloud"
}

variable "default_ttl" {
  type = "string"

  default = "300"
}
