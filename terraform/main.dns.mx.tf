resource "azurerm_dns_mx_record" "hic" {
  zone_name           = "${azurerm_dns_zone.hic.name}"
  resource_group_name = "${azurerm_resource_group.dns.name}"
  ttl                 = "${var.default_ttl}"

  name = "@"

  record {
    preference = "0"
    exchange   = "metjehoofdinde-cloud.mail.protection.outlook.com"
  }

  record {
    preference = "32767"
    exchange   = "ms30064265.msv1.invalid"
  }

  tags = "${var.tags}"
}
