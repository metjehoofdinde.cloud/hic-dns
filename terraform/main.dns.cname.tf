variable "hic_dns_cnames" {
  type = "map"

  default = {
    # office 365 entries
    "autodiscover" = "autodiscover.outlook.com."

    #azure entries
    "registry.k8s" = "hicacrk8s.azurecr.io."

    #sendgrid entries
    "em5544"        = "u7473576.wl077.sendgrid.net."
    "7473576"       = "sendgrid.net."
    "s1._domainkey" = "s1.domainkey.u7473576.wl077.sendgrid.net."
    "s2._domainkey" = "s2.domainkey.u7473576.wl077.sendgrid.net."
    "url1320"       = "sendgrid.net."

    "vote"                  = "ingress.k8s.metjehoofdinde.cloud."
    "vault"                 = "ingress.k8s.metjehoofdinde.cloud."
    "cwgroep11bot.telegram" = "ingress.k8s.metjehoofdinde.cloud."
    "todo"                  = "ingress.k8s.metjehoofdinde.cloud."
  }
}

resource "azurerm_dns_cname_record" "hic" {
  zone_name           = "${azurerm_dns_zone.hic.name}"
  resource_group_name = "${azurerm_resource_group.dns.name}"
  ttl                 = "${var.default_ttl}"

  count = "${length(keys(var.hic_dns_cnames))}"

  name   = "${element(keys(var.hic_dns_cnames),count.index)}"
  record = "${element(values(var.hic_dns_cnames),count.index)}"

  tags = "${var.tags}"
}
