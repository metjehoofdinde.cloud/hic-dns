output "name_servers" {
  value = "${azurerm_dns_zone.hic.name_servers}"
}

resource "local_file" "nameservers" {
  filename = "${path.module}/../transip/nameservers.txt"
  content  = "${join(",",azurerm_dns_zone.hic.name_servers)}"
}
