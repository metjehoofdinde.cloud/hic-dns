variable "hic_dns_a" {
  type = "map"

  default = {
    #kubernetes ingress
    "ingress.k8s" = "40.118.18.238"
  }
}

resource "azurerm_dns_a_record" "hic" {
  zone_name           = "${azurerm_dns_zone.hic.name}"
  resource_group_name = "${azurerm_resource_group.dns.name}"
  ttl                 = "${var.default_ttl}"

  count = "${length(keys(var.hic_dns_a))}"

  name    = "${element(keys(var.hic_dns_a),count.index)}"
  records = "${split(",",element(values(var.hic_dns_a),count.index))}"

  tags = "${var.tags}"
}
