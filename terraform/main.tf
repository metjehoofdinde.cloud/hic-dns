resource "azurerm_resource_group" "dns" {
  name     = "${var.resource_prefix}-dns"
  location = "${var.location}"

  tags = "${var.tags}"
}

resource "azurerm_dns_zone" "hic" {
  name                = "${var.dnsname}"
  resource_group_name = "${azurerm_resource_group.dns.name}"

  tags = "${var.tags}"
}
