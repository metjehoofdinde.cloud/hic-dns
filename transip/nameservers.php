#!/usr/bin/env php
<?php

require_once('lib/Transip/DomainService.php');

#get action
if (count($argv) <= 1){
  $action = 'list';
}else{
  $action = strtolower($argv[1]);
}

#test for actions
if( !in_array( $action , array('list','test','set') ) ){
  $error = $action . ' not in available actions [list|test|set]';
  echo $error;
  throw new Exception($error);
}

#connect to transip

define('USERNAME', getenv('TRANSIP_USERNAME'));
define('PRIVATE_KEY', getenv('TRANSIP_PRIVATE_KEY'));
define('DOMAIN', getenv('TRANSIP_DOMAIN'));

Transip_ApiSettings::$login=USERNAME;
Transip_ApiSettings::$privateKey= PRIVATE_KEY;

# get nameservers
$get_nameservers = Transip_DomainService::getInfo(DOMAIN)->nameservers;
#show current state
echo "nameservers:\r\n";
for($i=0; $i < count($get_nameservers); $i++){
  echo " ". $i . " | " . $get_nameservers[$i]->hostname  ."\r\n";
}

if( in_array( $action , array('test','set') ) ){
  #show desired state
  echo "\r\ndesired nameservers:\r\n";
  $set_nameserversRAW = file_get_contents(__DIR__ . '/nameservers.txt');
  $set_nameservers = preg_split('/,/' , $set_nameserversRAW , -1 , PREG_SPLIT_NO_EMPTY);
  
  for($i=0; $i < count($set_nameservers); $i++){
    $set_nameservers[$i] = rtrim($set_nameservers[$i],'.');
    echo " ". $i . " | " . $set_nameservers[$i]  ."\r\n";
  }

  #test for divergence
  $NSrecords_inDesiredState = 1;
  if(count($get_nameservers) == count($set_nameservers)){
    $nameservercount = count($get_nameservers);
    for($i=0; $i < $nameservercount; $i++){
      if($get_nameservers[$i]->hostname != $set_nameservers[$i]){
        $NSrecords_inDesiredState = 0;
      }
    }
  }else{
    $NSrecords_inDesiredState = 0;
  }

  if($NSrecords_inDesiredState == 0){
    echo "\r\nnameservers not in desired state\r\n";
    if($action == 'set'){
      #converge on desired state
      echo "\r\nconverging desired state\r\n ";      

      #wait for running actions to finish
      $CurrentDomainAction = Transip_DomainService::getCurrentDomainAction(DOMAIN);
      if(!$CurrentDomainAction->name == ""){
        $actionname = $CurrentDomainAction->name;
        echo "waiting for current action to finish:".  $actionname . "\r\n";
        $wait = 1;
        while($wait == 1){
          sleep(5);
          $CurrentDomainAction = Transip_DomainService::getCurrentDomainAction(DOMAIN);
          if($CurrentDomainAction->name == ""){
            $wait = 0;
            echo $actionname . " finished\r\n";
          }
          else{
            echo "  retrying...(5sec)\r\n";
          }
        }
      }

      
      #build list of desired servers
      $TransIPNameServers = array();
      for($i=0; $i < count($set_nameservers); $i++){
        $TransIPNameServers[] = new Transip_Nameserver($set_nameservers[$i]);
      }

      #set nameserver
      echo "Set nameservers\r\n";
      
      Transip_DomainService::setNameservers(DOMAIN ,$TransIPNameServers);
      $running = 1;
      while($running == 1){
        $CurrentDomainAction = Transip_DomainService::getCurrentDomainAction(DOMAIN);
        $actionname = 'changeNameservers';
        if($CurrentDomainAction == $actionname){
          if(!$CurrentDomainAction->hasFailed){
            echo "  running...(retrying in 5sec)\r\n";
            sleep(5);
          }else{
            $error = 'failed to set nameservers:'. $CurrentDomainAction->message. "\r\n";
            echo $error;
            throw new Exception($error);
          }
        }else{
          $running = 0;
          echo $actionname . " finished\r\n";
        }
      }
    }
  }else{
    echo "\r\nnameservers in desired state. No action required\r\n";
  }
}
?>